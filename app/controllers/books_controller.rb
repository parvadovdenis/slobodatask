class BooksController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :index]

  def index
    @books = Book.all
  end

  def new
    @book = Book.new

  end

  def create
    @book = current_user.books.build(book_params)
    @book.save
    redirect_to books_path
  end

  def show
    @book = Book.find(params[:id])
  end

  def update
    @book = Book.find(params[:id])
    if @book.update(book_params)
      redirect_to @book

    else
      render 'edit'
    end
  end

  def edit
    @book = Book.find(params[:id])
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    redirect_to books_path
  end

  private
  def book_params
    params.require(:book).permit(:title, :author, :content, :category_novel, :category_fantasy,
                                 :category_historical, :category_biography, :image, :draft)

  end
end
