class Book < ApplicationRecord
  dragonfly_accessor :image do
    default Rails.root.join('public', 'images', 'default', 'default.png')

  end
  belongs_to :user

end
