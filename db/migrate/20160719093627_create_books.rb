class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.text :title
      t.text :author
      t.text :title
      t.boolean :draft
      t.boolean :category_novel
      t.boolean :category_historical
      t.boolean :category_fantasy
      t.boolean :category_biography
      t.string :image_uid
      t.integer :user_id
      t.timestamps
    end
  end
end
