class AssociateBooksWithUser < ActiveRecord::Migration
  def change
    change_table :books do |t|
      t.references :users, foreign_key: true
    end
  end
end
